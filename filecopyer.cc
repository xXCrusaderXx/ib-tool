#include "filecopyer.h"
#include <QtCore/qdebug.h>

FileCopyer::FileCopyer(FileManager* fManager, QThread* thread, Logger* myLogger)
    : QObject(nullptr)
    , _fManager(fManager)
    , _thread(thread)
    , _myLogger(myLogger)
{
    setChunkSize(DEFAULT_CHUNK_SIZE);
}

FileCopyer::~FileCopyer() { thread()->quit(); }

void FileCopyer::copy()
{
    for (const auto& file : paths) total = total + QFileInfo(file.src_path).size();
    qInfo() << QStringLiteral("%1 bytes should be write in total").arg(total);

    int indx = 0;
    qInfo() << QStringLiteral("writing with chunk size of %1 byte").arg(chunkSize());

    for (auto file : paths)
    {
        emit _fManager->jobCounter(paths.size(), indx);

        QFile srcFile(file.src_path);
        QFile dstFile(file.dst_path);

        if (QFile::exists(file.dst_path))
        {
            _myLogger->out_string(file.dst_path + "-> does not exist!");
            QFile::remove(file.dst_path);
        }

        if (!srcFile.open(QFileDevice::ReadOnly))
        {
            _myLogger->out_string(file.src_path + "-> does not exist!");
            continue;
        }

        if (!dstFile.open(QFileDevice::WriteOnly))
        {
            _myLogger->out_string("can not write: " + file.dst_path);
            continue;
        }

        qint64 fSize = srcFile.size();
        while (fSize > 0 && !(_fManager->stopThread == true))
        {
            const auto data     = srcFile.read(chunkSize());
            const auto _written = dstFile.write(data);
            if (data.size() == _written)
            {
                written += _written;
                fSize -= data.size();

                emit _fManager->copyProgress(total, written);

                qInfo() << QStringLiteral("written:").arg(written).arg(total);
            }
            else
            {
                qWarning() << QStringLiteral("failed to write to %1 (error:%2)").arg(dstFile.fileName()).arg(dstFile.errorString());
                fSize = 0;
                break;
            }
        }

        srcFile.close();
        dstFile.close();

        indx++;
    }
    emit _fManager->jobCounter(paths.size(), indx);
    emit _fManager->copyerFinished(true);
}

void FileCopyer::start_copy() { copy(); }
