#include "guicreator.h"

GuiCreator::GuiCreator()
{
    // ui->setWindowTitle("Log pick up");
    // ui->setFixedSize(600, 300);
}

void GuiCreator::setAllElements(MainWindow* ui)
{
    ui->setWindowTitle("Log pick up");
    ui->setFixedSize(600, 300);

    pushButtonGo = new QPushButton(ui);
    pushButtonGo->setGeometry(410, 70, 160, 35);
    pushButtonGo->setText("Copy");

    pushButtonCancel = new QPushButton(ui);
    pushButtonCancel->setGeometry(410, 70, 160, 35);
    pushButtonCancel->setText("Cancel");
    pushButtonCancel->setHidden(true);

    switch_to_copy = new QPushButton(ui);
    switch_to_copy->setGeometry(170, 55, 40, 40);
    switch_to_copy->setText(">>");

    switch_out_off_copy = new QPushButton(ui);
    switch_out_off_copy->setGeometry(170, 95, 40, 40);
    switch_out_off_copy->setText("<<");

    labelFrom = new QLabel(ui);
    labelFrom->setGeometry(410, 15, 50, 25);
    labelFrom->setText("from:");

    labelTo = new QLabel(ui);
    labelTo->setGeometry(410, 40, 50, 25);
    labelTo->setText("to:");

    labelBytes = new QLabel(ui);
    labelBytes->setGeometry(410, 115, 160, 25);

    labelJobs = new QLabel(ui);
    labelJobs->setGeometry(410, 140, 160, 25);

    dateStart = new QDateEdit(ui);
    dateStart->setGeometry(460, 15, 100, 25);
    dateStart->setDate(QDate::currentDate());
    dateStart->setMaximumDate(QDate::currentDate());

    dateEnd = new QDateEdit(ui);
    dateEnd->setGeometry(460, 40, 100, 25);
    dateEnd->setDate(QDate::currentDate());
    dateEnd->setMaximumDate(QDate::currentDate());

    progressBar = new QProgressBar(ui);
    progressBar->setGeometry(10, 210, 400, 30);
}
