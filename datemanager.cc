#include "datemanager.h"
#include "QMessageBox"

DateManager::DateManager(QDate startDate, QDate endDate)
    : _startDate(startDate)
    , _endDate(endDate)
{
    if (check_for_illegal_date(_startDate, _endDate))
    {
        _hapComDateList.clear();
        _xgvDateList.clear();
        return;
    }
}

std::vector<QString> DateManager::getDateListHapCom()
{
    if (_startDate.month() == _endDate.month())
    {
        int dayCounter = _startDate.day();
        do
        {
            QString dayCorrect;
            if (dayCounter < 10)
            {
                QString tmp;
                dayCorrect = "0" + tmp.setNum(dayCounter);
            }
            else
            {
                QString tmp;
                dayCorrect = tmp.setNum(dayCounter);
            }

            int     month = _startDate.month();
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_endDate.year()) + MonthCorrect + dayCorrect;
            _hapComDateList.push_back(fullDate);

            dayCounter++;
        } while (dayCounter <= _endDate.day());
    }

    else if (_startDate.month() <= _endDate.month())
    {
        int startDay = _startDate.day();
        do
        {
            QString dayC;
            if (startDay < 10)
            {
                QString tmp;
                dayC = "0" + tmp.setNum(startDay);
            }
            else
            {
                QString tmp;
                dayC = tmp.setNum(startDay);
            }

            int     month = _startDate.month();
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_startDate.year()) + MonthCorrect + dayC;
            _hapComDateList.push_back(fullDate);

            ++startDay;
        } while (startDay <= monthDays.at(static_cast<size_t>(_startDate.month())));

        startDay = 1;
        do
        {
            QString dayC;
            if (startDay < 10)
            {
                QString tmp;
                dayC = "0" + tmp.setNum(startDay);
            }
            else
            {
                QString tmp;
                dayC = tmp.setNum(startDay);
            }

            int     month = _startDate.month() + 1;
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_endDate.year()) + MonthCorrect + dayC;

            _hapComDateList.push_back(fullDate);

            startDay++;
        } while (startDay <= _endDate.day());
    }
    return _hapComDateList;
}

std::vector<QString> DateManager::getDateListXGV()
{
    if (_startDate.month() == _endDate.month())
    {
        int dayCounter = _startDate.day();
        do
        {
            QString dayCorrect;
            if (dayCounter < 10)
            {
                QString tmp;
                dayCorrect = "0" + tmp.setNum(dayCounter);
            }
            else
            {
                QString tmp;
                dayCorrect = tmp.setNum(dayCounter);
            }

            int     month = _startDate.month();
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_endDate.year()) + "-" + MonthCorrect + "-" + dayCorrect;
            _xgvDateList.push_back(fullDate);

            dayCounter++;
        } while (dayCounter <= _endDate.day());
    }

    else if (_startDate.month() <= _endDate.month())
    {
        int startDay = _startDate.day();
        do
        {
            QString dayC;
            if (startDay < 10)
            {
                QString tmp;
                dayC = "0" + tmp.setNum(startDay);
            }
            else
            {
                QString tmp;
                dayC = tmp.setNum(startDay);
            }

            int     month = _startDate.month();
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_startDate.year()) + MonthCorrect + dayC;
            _xgvDateList.push_back(fullDate);

            ++startDay;
        } while (startDay <= monthDays.at(static_cast<size_t>(_startDate.month())));

        startDay = 1;
        do
        {
            QString dayC;
            if (startDay < 10)
            {
                QString tmp;
                dayC = "0" + tmp.setNum(startDay);
            }
            else
            {
                QString tmp;
                dayC = tmp.setNum(startDay);
            }

            int     month = _startDate.month() + 1;
            QString MonthCorrect;
            if (month < 10)
            {
                QString tmp;
                MonthCorrect = "0" + tmp.setNum(month);
            }
            else
            {
                QString tmp;
                MonthCorrect = tmp.setNum(month);
            }

            QString tmp1;
            QString fullDate = tmp1.setNum(_endDate.year()) + MonthCorrect + dayC;

            _xgvDateList.push_back(fullDate);

            startDay++;
        } while (startDay <= _endDate.day());
    }
    return _xgvDateList;
}

bool DateManager::check_for_illegal_date(QDate _startDate, QDate _endDate)
{
    if (_startDate.day() > _endDate.day())
    {
        QMessageBox msgBox;
        msgBox.setText("illegal start date (day)!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();

        return true;
    }

    if (_startDate.month() > _endDate.month())
    {
        QMessageBox msgBox;
        msgBox.setText("illegal start date (month)!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();

        return true;
    }

    if (_startDate.year() > _endDate.year())
    {
        QMessageBox msgBox;
        msgBox.setText("illegal start date (year)!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();

        return true;
    }
}
