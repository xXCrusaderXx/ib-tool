#pragma once

#include "filezipper.h"
#include "structs.h"
#include <QtCore/qfile.h>
#include <QtCore/qfileinfo.h>
#include <QtCore/qobject.h>
#include <QtCore/qstring.h>
#include <QtCore/qthread.h>
#include <QtCore/qvector.h>
#include <filemanager.h>

class FileCopyer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint64 chunksize READ chunkSize WRITE setChunkSize)
    Q_PROPERTY(QVector<QString> sourcePaths READ sourcePaths WRITE setSourcePaths)
    Q_PROPERTY(QVector<QString> destinationPaths READ destinationPaths WRITE setDestinationPaths)

public:
    static const int DEFAULT_CHUNK_SIZE = 1024 * 1024 * 1;

    FileCopyer(FileManager* fManager, QThread* thread, Logger* myLogger);
    ~FileCopyer();

    qint64 chunkSize() const { return _chunk; }
    void   setChunkSize(qint64 ch) { _chunk = ch; }

    std::vector<fileCopyData> allPaths() const { return paths; }
    void                      setPaths(const std::vector<fileCopyData>& _paths) { paths = _paths; }

    QVector<QString> sourcePaths() const { return src; }
    void             setSourcePaths(const QVector<QString>& _src) { src = _src; }

    QVector<QString> destinationPaths() const { return dst; }
    void             setDestinationPaths(const QVector<QString>& _dst) { dst = _dst; }

    void start_copy();

private:
    QVector<QString>          src, dst;
    std::vector<fileCopyData> paths;
    qint64                    _chunk;
    qint64                    total   = 0;
    qint64                    written = 0;
    FileManager*              _fManager;
    Logger*                   _myLogger;

    QThread* _thread;
    void     copy();
};
