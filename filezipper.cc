#include "filezipper.h"
#include <QtCore/qdebug.h>

fileZipper::fileZipper() {}

void unzipFile(QString fileInput, QString fileOutput)
{
    QFile fi(fileInput);
    QFile fo(fileOutput);

    if (fi.open(QFile::ReadOnly) && fo.open(QFile::WriteOnly))
    {
        int compress_level = 9;                            // compression level
        fo.write(qCompress(fi.readAll(), compress_level)); // read input, compress and write to output is a single line of code
        fi.close();
        fo.close();
    }
}

void zipFile(QString fileInput, QString fileOutput)
{
    QFile fi(fileInput);
    QFile fo(fileOutput);

    if (fi.open(QFile::ReadOnly) && fo.open(QFile::WriteOnly))
    {
        int compress_level = 9;                            // compression level
        fo.write(qCompress(fi.readAll(), compress_level)); // read input, compress and write to output is a single line of code
        fi.close();
        fo.close();
    }
}
