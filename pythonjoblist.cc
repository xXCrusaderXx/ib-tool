#include "pythonjoblist.h"

PythonJobList::PythonJobList(QString LogName)
    : _filename(LogName)
{}

void PythonJobList::out_string(QString out)
{
    QFile file(_filename);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);

        do
            stream.readAll();
        while (not stream.atEnd());
        stream << out << endl;
    }
}
