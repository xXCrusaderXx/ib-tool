#pragma once

#include "QDate"
#include "structs.h"

class DateManager
{
public:
    DateManager(QDate _startDate, QDate _endDate);
    std::vector<QString> getDateListHapCom();
    std::vector<QString> getDateListXGV();
    std::vector<QString> getDateListFolder() { return _dateListFolder; }

private:
    std::vector<QString> _hapComDateList;
    std::vector<QString> _xgvDateList;
    std::vector<QString> _dateListFolder;

    QDate _startDate;
    QDate _endDate;

    bool check_for_illegal_date(QDate, QDate);

    const std::vector<int> monthDays{ 31, 30, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
};
