#pragma once

#include "QString"
#include "logger.h"
#include "structs.h"
#include <QtCore/qthread.h>

class FileManager : public QObject
{
    Q_OBJECT

public:
    FileManager(QThread* thread, Logger* myLogger);
    ~FileManager();

    void makeCopyListHapCom(std::vector<QString> dateListNormal, std::vector<agvSrc> hapComList, std::vector<QString> dstPath);
    void makeCopyListXgv(std::vector<QString> dateListXGV, std::vector<agvSrc> xgvList, std::vector<QString> dstPath);

    void copyFiles();
    void cancelThread() { stopThread = true; }

    bool stopThread = false;

private:
    std::vector<fileCopyData> _copyListHapCom;
    std::vector<fileCopyData> _copyListXgv;

    QThread* _thread;
    Logger*  _myLogger;

signals:
    void copyerFinished(bool success);
    void copyProgress(int max, int value);
    void jobCounter(int total, int done);
};
