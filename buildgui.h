#ifndef BUILDGUI_H
#define BUILDGUI_H

#include "QLabel"
#include "QRadioButton"
#include "QTabWidget"
#include "mainwindow.h"
#include "structs.h"

class BuildGui
{
private:
    std::vector<tabsStruct> tabs;

public:
    BuildGui();
    void                    setupWindow(MainWindow* MW);
    void                    setupObjects(MainWindow* MW);
    void                    setupTabs(MainWindow* MW, std::vector<agvSrc> agvs);
    std::vector<tabsStruct> getTabsInfo();
};

#endif // BUILDGUI_H
