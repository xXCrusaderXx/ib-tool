#include "mainwindow.h"
#include "QDate"
#include "QFile"
#include "QFileDialog"
#include "QMessageBox"
#include "QThread"
#include "structs.h"
#include "ui_mainwindow.h"
#include "windows.h"
#include <QtCore/qdebug.h>
#include <agvxmlwriter.h>
#include <xmlreader.h>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Log Grapper " + version);
    this->setFixedSize(600, 400);

    pushButtonGo = new QPushButton(this);
    pushButtonGo->setGeometry(410, 120, 160, 35);
    pushButtonGo->setText("Copy");

    pushButtonCancel = new QPushButton(this);
    pushButtonCancel->setGeometry(410, 120, 160, 35);
    pushButtonCancel->setText("Cancel");
    pushButtonCancel->setHidden(true);

    switch_to_copy = new QPushButton(this);
    switch_to_copy->setGeometry(170, 105, 40, 40);
    switch_to_copy->setText("->");

    switch_all = new QPushButton(this);
    switch_all->setGeometry(170, 145, 40, 40);
    switch_all->setText("<->");

    switch_out_off_copy = new QPushButton(this);
    switch_out_off_copy->setGeometry(170, 185, 40, 40);
    switch_out_off_copy->setText("<-");

    labelFrom = new QLabel(this);
    labelFrom->setGeometry(410, 65, 50, 25);
    labelFrom->setText("from:");

    labelTo = new QLabel(this);
    labelTo->setGeometry(410, 90, 50, 25);
    labelTo->setText("to:");

    labelBytes = new QLabel(this);
    labelBytes->setGeometry(410, 165, 160, 25);

    labelJobs = new QLabel(this);
    labelJobs->setGeometry(410, 190, 160, 25);

    dateStart = new QDateEdit(this);
    dateStart->setGeometry(460, 65, 100, 25);
    dateStart->setDate(QDate::currentDate());
    dateStart->setMaximumDate(QDate::currentDate());

    dateEnd = new QDateEdit(this);
    dateEnd->setGeometry(460, 90, 100, 25);
    dateEnd->setDate(QDate::currentDate());
    dateEnd->setMaximumDate(QDate::currentDate());

    progressBar = new QProgressBar(this);
    progressBar->setGeometry(10, 260, 400, 30);

    int xW   = 150;
    int yW   = 200;
    agv_list = new QListWidget(this);
    agv_list->setGeometry(10, 55, xW, yW);
    agv_list->setSortingEnabled(true);
    copy_list = new QListWidget(this);
    copy_list->setGeometry(xW + 70, 55, xW, yW);
    copy_list->setSortingEnabled(true);

    XmlReader            myReader(configPath);
    std::vector<QString> customers = myReader.getCustomers();

    customerBox = new QComboBox(this);
    customerBox->setGeometry(10, 25, 150, 20);
    for (QString customer : customers) { customerBox->addItem(customer); }

    helpMenu = menuBar()->addMenu("Hilfe");
    helpMenu->addAction("Version: " + version);

    QString logDate = "", logTime = "", tmp = "";
    QDate   date = QDate::currentDate();
    logDate += tmp.setNum(date.year());
    if (date.month() <= 10)
    {
        QString month = "0" + tmp.setNum(date.month());
        logDate += month;
    }
    else
        logDate += tmp.setNum(date.month());

    if (date.day() <= 10)
    {
        QString day = "0" + tmp.setNum(date.day());
        logDate += day;
    }
    logDate += tmp.setNum(date.day());

    QTime time = QTime::currentTime();
    if (time.hour() <= 10)
    {
        QString hour = "0" + tmp.setNum(time.hour());
        logTime += hour;
    }
    else
        logTime += tmp.setNum(time.hour());

    if (time.minute() <= 10)
    {
        QString min = "0" + tmp.setNum(time.minute());
        logTime += min;
    }
    else
        logTime += tmp.setNum(time.minute());

    if (time.second() <= 10)
    {
        QString sec = "0" + tmp.setNum(time.second());

        logTime += sec;
    }
    else
        logTime += tmp.setNum(time.second());

    myLogger = new Logger(logDate + "_" + logTime + "_debug.log");
    myLogger->out_string("\nnew session started\n");

    myPythonJobs = new PythonJobList("pythonTaskList.txt");

    hapComList.clear();
    xgvList.clear();
    hapComList = myReader.getHapComPathList();
    xgvList    = myReader.getXgvPathList();
    preDevDst  = myReader.getSavedDstPath();

    myLogger->out_string("hapComList:");
    myLogger->out_agvSrc(hapComList);
    myLogger->out_string("xgvList:");
    myLogger->out_agvSrc(xgvList);
    myLogger->out_string("\npreset destination:");
    myLogger->out_string(preDevDst);
    myPythonJobs->out_string(preDevDst + "\\Log\\");

    RadioButtonSaveDst = new QRadioButton(this);
    RadioButtonSaveDst->setGeometry(10, 290, 300, 35);
    if (preDevDst == "") { RadioButtonSaveDst->setText("no preset destination"); }
    else
    {
        RadioButtonSaveDst->setText("save at: " + preDevDst);
    }

    QObject::connect(customerBox, SIGNAL(activated(QString*)), this, SLOT(customerChoosen(QString*)));
    QObject::connect(pushButtonGo, SIGNAL(clicked()), this, SLOT(go_clicked()));
    QObject::connect(pushButtonCancel, SIGNAL(clicked()), this, SLOT(cancelCopyer()));
    QObject::connect(switch_to_copy, SIGNAL(clicked()), this, SLOT(switch_right()));
    QObject::connect(switch_out_off_copy, SIGNAL(clicked()), this, SLOT(switch_left()));
    QObject::connect(switch_all, SIGNAL(clicked()), this, SLOT(switch_all_to()));

    QObject::connect(agv_list, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClickedAgv(QListWidgetItem*)));
    QObject::connect(copy_list, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClickedCopy(QListWidgetItem*)));
    QObject::connect(agv_list, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(agvItemDoubleClickedS(QListWidgetItem*)));
    QObject::connect(copy_list, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(copyItemDoubleClickedS(QListWidgetItem*)));

    ui->statusbar->showMessage(version, 5000);
}

void MainWindow::go_clicked()
{
    if (dateStart->date() > dateEnd->date())
    {
        startMessageBox("begin date ist bigger then end date!");
        myLogger->out_string("begin date ist bigger then end date!");
        return;
    }

    if (RadioButtonSaveDst->isChecked() == false)
    {
        preDevDst = QFileDialog::getExistingDirectory(this, tr("choose save direction"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        myLogger->out_string("RadioButton pre destintaion = false");
    }

    if (RadioButtonSaveDst->isChecked() && preDevDst == "")
    {
        preDevDst = QFileDialog::getExistingDirectory(this, tr("choose save direction"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        myLogger->out_string("RadioButton pre destintaion = true");
    }

    if (preDevDst == "") { return; }

    myLogger->out_string("RadioButton pre destintaion = true");
    pushButtonGo->setHidden(true);
    pushButtonCancel->setHidden(false);
    startFileManagement(preDevDst);
    ui->statusbar->showMessage("copyer started !", 5000);
}

void MainWindow::startFileManagement(QString preDevDst)
{
    myLogger->out_string("\nnew try:");
    DateManager          myDateM(dateStart->date(), dateEnd->date());
    std::vector<QString> hapComDateList = myDateM.getDateListHapCom();
    std::vector<QString> xgvDateList    = myDateM.getDateListXGV();

    myLogger->out_string("dates in hapComList:");
    for (auto date : hapComDateList)
    {
        myLogger->out_string(date);
        myPythonJobs->out_string(date);
    }
    myLogger->out_string("dates in xgvDateList:");
    for (auto date : xgvDateList) { myLogger->out_string(date); }

    copyJobListHapCom.clear();
    copyJobListXGV.clear();
    myLogger->out_string("agv copy list:");
    for (auto agv : hapComList)
    {
        QList<QListWidgetItem*> itemL;
        itemL    = copy_list->findItems(agv.name, Qt::MatchContains);
        int size = itemL.size();
        if (size > 0 && agv.mode == "HapCom")
        {
            copyJobListHapCom.push_back(agv);
            myLogger->out_string(agv.name);
        }
    }

    for (auto agv : xgvList)
    {
        QList<QListWidgetItem*> itemL;
        itemL    = copy_list->findItems(agv.name, Qt::MatchContains);
        int size = itemL.size();
        if (size > 0 && agv.mode == "XGV")
        {
            copyJobListXGV.push_back(agv);
            myLogger->out_string(agv.name);
        }
    }

    FolderManager        myFolderManager;
    std::vector<QString> HapComDstPathList = myFolderManager.creatDirectory(preDevDst, hapComDateList, copyJobListHapCom);
    std::vector<QString> XGVDstPathList    = myFolderManager.creatDirectory(preDevDst, hapComDateList, copyJobListXGV);

    myLogger->out_string("Hapcom destination paths:");
    for (auto dst : HapComDstPathList) { myLogger->out_string(dst); }
    myLogger->out_string("XGV destination paths:");
    for (auto dst : XGVDstPathList) { myLogger->out_string(dst); }

    local     = new QThread;
    myManager = new FileManager(local, myLogger);
    QObject::connect(myManager, SIGNAL(copyProgress(int, int)), this, SLOT(updateProgressBar(int, int)));
    QObject::connect(myManager, SIGNAL(copyerFinished(bool)), this, SLOT(copyerFinished(bool)));
    QObject::connect(myManager, SIGNAL(jobCounter(int, int)), this, SLOT(jobCounter(int, int)));

    myLogger->out_string("\nhapCom copy list:");
    myManager->makeCopyListHapCom(hapComDateList, copyJobListHapCom, HapComDstPathList);

    myLogger->out_string("\nXGV copy list:");
    myManager->makeCopyListXgv(xgvDateList, copyJobListXGV, XGVDstPathList);

    myLogger->out_string("\ncopyer started!");
    local->start();
}

void MainWindow::startMessageBox(QString text)
{
    QMessageBox msgBox;
    msgBox.setText(text);
    msgBox.exec();
    return;
}

void MainWindow::updateProgressBar(int total, int value)
{
    progressBar->setMaximum(total);
    progressBar->setValue(value);
    QString tmp, tmp1;
    labelBytes->setText("bytes: " + tmp.setNum(value / 1000000) + "/" + tmp1.setNum(total / 1000000) + " Mb");
}

void MainWindow::copyerFinished(bool success)
{
    if (success == true)
    {
        pushButtonGo->setHidden(false);
        pushButtonCancel->setHidden(true);
        progressBar->setValue(100);
        progressBar->setMaximum(100);
        ui->statusbar->showMessage("copyer finished!", 2000);
        myManager->~FileManager();
    }

    // Python-script-starter

    /*
    #include <cstdlib>
    #include <iostream>
    #include <string.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <unistd.h>

        std::string  strPath  = "python_starter.ps1";
        std::wstring WstrPath = std::wstring(strPath.begin(), strPath.end());

        if (_waccess(WstrPath.c_str(), 0) == 0)
        {
            system("start powershell.exe Set-ExecutionPolicy RemoteSigned \n");
            system("start powershell.exe python_starter.ps1");
            system("cls");
        }
        else
        {
            system("cls");
            system("pause");
        }
        */
}

void MainWindow::jobCounter(int total, int done)
{
    QString tmp1, tmp2;
    labelJobs->setText("progress: " + tmp1.setNum(done) + "/" + tmp2.setNum(total));
}

void MainWindow::cancelCopyer()
{
    ui->statusbar->showMessage("copyer canceled!", 2000);
    myManager->stopThread = true;
}

void MainWindow::itemClickedAgv(QListWidgetItem* item) { myItemAgvList = item->text(); }
void MainWindow::itemClickedCopy(QListWidgetItem* item) { myItemCopyList = item->text(); }

void MainWindow::agvItemDoubleClickedS(QListWidgetItem* item)
{
    QList<QListWidgetItem*> itemL;
    itemL    = copy_list->findItems(myItemAgvList, Qt::MatchExactly);
    int size = itemL.size();
    if (size == 0) { copy_list->addItem(item->text()); }
}

void MainWindow::copyItemDoubleClickedS(QListWidgetItem* item)
{
    int itemRow = copy_list->row(item);
    copy_list->takeItem(itemRow);
}

void MainWindow::switch_right()
{
    QList<QListWidgetItem*> itemL;
    itemL    = copy_list->findItems(myItemAgvList, Qt::MatchExactly);
    int size = itemL.size();
    if (size == 0) { copy_list->addItem(myItemAgvList); }

    copy_list->sortItems(Qt::AscendingOrder);
}

void MainWindow::switch_left()
{
    QList<QListWidgetItem*> itemL;
    itemL = copy_list->findItems(myItemCopyList, Qt::MatchExactly);

    for (auto itemW : itemL)
    {
        int itemRow = copy_list->row(itemW);
        copy_list->takeItem(itemRow);
    }
}

void MainWindow::switch_all_to()
{
    QList<QListWidgetItem*> items_copy_list;
    QList<QListWidgetItem*> items_agv_list;
    items_copy_list = copy_list->findItems("AGV", Qt::MatchContains);
    items_agv_list  = agv_list->findItems("AGV", Qt::MatchContains);

    int size = items_copy_list.size();
    if (size == 0)
    {
        for (auto itemW : items_agv_list)
        {
            auto text = itemW->text();
            copy_list->addItem(text);
        }
    }

    for (auto itemW : items_copy_list)
    {
        int itemRow = copy_list->row(itemW);
        copy_list->takeItem(itemRow);
    }
}

void MainWindow::CustomerChoosen(QString* customer)
{
    for (auto agv : hapComList)
    {
        QString listName;
        listName = agv.name;
        listName = listName + " (" + agv.mode + ")";
        if (agv.customer == customer) { agv_list->addItem(listName); }
    }

    for (auto agv : xgvList)
    {
        QString listName;
        listName = agv.name;
        listName = listName + " (" + agv.mode + ")";
        if (agv.customer == customer) { agv_list->addItem(listName); }
    }
}

MainWindow::~MainWindow() { delete ui; }
