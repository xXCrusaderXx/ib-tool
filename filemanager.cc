#include "filemanager.h"
#include "filecopyer.h"

FileManager::FileManager(QThread* thread, Logger* mylogger)
    : QObject(nullptr)
    , _thread(thread)
    , _myLogger(mylogger)
{
    moveToThread(_thread);
    QObject::connect(_thread, &QThread::started, this, &FileManager::copyFiles);
}

FileManager::~FileManager() {}

void FileManager::makeCopyListHapCom(std::vector<QString> dateListHapCom, std::vector<agvSrc> hapComList, std::vector<QString> dstPath)
{
    std::vector<QString> fileTypes{ "Debug_", "ErrorLog_" };
    fileCopyData         FileData;

    _copyListHapCom.clear();

    int c = 0;
    for (auto date : dateListHapCom)
    {
        for (auto agv : hapComList)
        {
            if (agv.mode == "HapCom")
            {
                for (auto fileType : fileTypes)
                {
                    QString fullSrcName = agv.path + fileType + date + ".log";
                    _myLogger->out_string("data src: " + fullSrcName);
                    QString fullDstName = dstPath.at(c) + "\\" + fileType + date + ".log";
                    _myLogger->out_string("data dst: " + fullDstName);
                    FileData.src_path = fullSrcName;
                    FileData.dst_path = fullDstName;
                    _copyListHapCom.push_back(FileData);
                }
                c++;
            }
        }
    }
}

void FileManager::makeCopyListXgv(std::vector<QString> dateListXGV, std::vector<agvSrc> xgvList, std::vector<QString> dstPath)
{
    std::vector<QString> xgvIndex{ "_01", "_02", "_03", "_04", "_05", "_06", "_07", "_08", "_09", "_10", "_11", "_12", "_13", "_14", "_15", "_16", "_17", "_18", "_19", "_20", "_21", "_22", "_23" };
    fileCopyData         FileData;

    _copyListXgv.clear();

    QString path;

    int c = 0;
    for (auto agv : xgvList)
    {
        if (agv.mode == "XGV")
        {
            path = agv.path + "\\";
            QDir        directory_logs(path);
            QStringList files_logs = directory_logs.entryList(
                QStringList() << "*_debug.log"
                              << "*_error.log",
                QDir::Files);

            path = agv.path + "\\archiv\\";
            QDir        directory_archiv(path);
            QStringList files_archiv = directory_archiv.entryList(
                QStringList() << "*_debug.log"
                              << "*_error.log",
                QDir::Files);

            foreach (QString filename, files_logs)
            {
                for (auto number : xgvIndex)
                {
                    for (auto date : dateListXGV)
                    {
                        if (filename.contains(date) && (filename.contains(number)) && (filename.contains("debug")))
                        {
                            QString fullSrcName = path + filename;
                            _myLogger->out_string("src: " + fullSrcName);
                            QString fullDstName = dstPath.at(c) + "\\" + filename;
                            _myLogger->out_string("dst: " + fullDstName);
                            FileData.src_path = fullSrcName;
                            FileData.dst_path = fullDstName;
                            _copyListXgv.push_back(FileData);
                        }
                    }
                }
            }
            foreach (QString filename, files_archiv)
            {
                for (auto number : xgvIndex)
                {
                    for (auto date : dateListXGV)
                    {
                        if (filename.contains(date) && (filename.contains(number)) && (filename.contains("debug")))
                        {
                            QString fullSrcName = path + filename;
                            _myLogger->out_string("src: " + fullSrcName);
                            QString fullDstName = dstPath.at(c) + "\\" + filename;
                            _myLogger->out_string("dst: " + fullDstName);
                            FileData.src_path = fullSrcName;
                            FileData.dst_path = fullDstName;
                            _copyListXgv.push_back(FileData);
                        }
                    }
                }
            }
        }
        c++;
    }
}

void FileManager::copyFiles()
{
    std::vector<fileCopyData> completeCopyList;
    completeCopyList.clear();
    for (auto job : _copyListHapCom) { completeCopyList.push_back(job); }
    for (auto job : _copyListXgv) { completeCopyList.push_back(job); }

    int fullSize = 0;
    for (auto file : completeCopyList) { fullSize = fullSize + file.src_path.size(); }

    emit copyProgress(0, 0);

    auto worker = new FileCopyer(this, _thread, _myLogger);
    worker->setPaths(completeCopyList);
    worker->start_copy();

    while (stopThread == false) {}

    worker->~FileCopyer();
}
