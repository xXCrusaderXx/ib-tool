#pragma once

#include "QFile"
#include "QString"
#include "QTextStream"
#include "structs.h"
#include "vector"
#include "windows.h"

class Logger
{
public:
    Logger(QString logName);
    void out_string(QString out);
    void out_agvSrc(std::vector<agvSrc> out);

    QString _filename;
    QString dstPath;
};
