#pragma once

#include "structs.h"
#include "vector"
#include "windows.h"
#include <QString>

class FolderManager
{
public:
    FolderManager();
    std::vector<QString> creatDirectory(QString dstPath, std::vector<QString> dateList, std::vector<agvSrc> copyJobList);
    std::vector<QString> getDstPathList();

private:
    std::vector<QString> dstPathList;
};
