#pragma once

#include "QString"

class CustomerSelection
{
public:
    CustomerSelection();
    QString getConfigurationDirectory() { return configPath; }
    QString getChoosenCustomer() { return choosenCustomer; }

private:
    QString configPath      = "\\config\\config.xml";
    QString choosenCustomer = "";
    QString gf_dd           = "Globalfoundries";
    QString siltronic_fg    = "Siltronic";
};
