#pragma once

#include "QDir"
#include "QString"

class fileZipper
{
public:
    fileZipper();

    void unzipFile(QString fileInput, QString fileOutput);
    void zipFile(QString fileInput, QString fileOutput);
};
