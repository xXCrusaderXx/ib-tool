#pragma once

#include "QFile"
#include "QString"
#include "QTextStream"
#include "structs.h"
#include "vector"

class PythonJobList
{
public:
    PythonJobList(QString logName);
    void out_string(QString out);

    QString _filename;
};
