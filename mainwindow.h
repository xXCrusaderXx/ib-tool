#pragma once

#include "QComboBox"
#include "QDateEdit"
#include "QLabel"
#include "QListWidget"
#include "QProgressBar"
#include "QPushButton"
#include "QRadioButton"
#include "pythonjoblist.h"
#include <QMainWindow>
#include <QMessageLogger>
#include <datemanager.h>
#include <filemanager.h>
#include <foldermanager.h>
#include <guicreator.h>
#include <logger.h>
#include <structs.h>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

    QPushButton*  pushButtonGo;
    QPushButton*  pushButtonCancel;
    QPushButton*  switch_to_copy;
    QPushButton*  switch_out_off_copy;
    QPushButton*  switch_all;
    QRadioButton* RadioButtonSaveDst;
    QLabel*       labelFrom;
    QLabel*       labelTo;
    QLabel*       labelBytes;
    QLabel*       labelJobs;
    QDateEdit*    dateStart;
    QDateEdit*    dateEnd;
    QProgressBar* progressBar;
    QListWidget*  agv_list;
    QListWidget*  copy_list;
    QComboBox*    customerBox;

    Logger*        myLogger;
    PythonJobList* myPythonJobs;

private:
    Ui::MainWindow*     ui;
    QString             version    = "v.1.6";
    QString             configPath = "\\config\\config.xml";
    std::vector<agvSrc> hapComList;
    std::vector<agvSrc> xgvList;
    std::vector<agvSrc> copyJobListHapCom;
    std::vector<agvSrc> copyJobListXGV;
    QString             preDevDst;
    QString             myItemAgvList;
    QString             myItemCopyList;

    QTabWidget*  myTabWidget;
    QThread*     local;
    FileManager* myManager;

    void startFileManagement(QString dstPath);
    void startMessageBox(QString text);

    QMenu* helpMenu;

signals:
    void itemClicked(QListWidgetItem* item);
    void itemDoubleClicked(QListWidgetItem* item);

private slots:
    void go_clicked();
    void copyerFinished(bool success);
    void updateProgressBar(int total, int value);
    void jobCounter(int total, int done);
    void cancelCopyer();
    void switch_right();
    void switch_left();
    void switch_all_to();
    void itemClickedAgv(QListWidgetItem* item);
    void itemClickedCopy(QListWidgetItem* item);
    void agvItemDoubleClickedS(QListWidgetItem* item);
    void copyItemDoubleClickedS(QListWidgetItem* item);

    void CustomerChoosen(QString*);
};
