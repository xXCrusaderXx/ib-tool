#include "foldermanager.h"
#include "QFile"

FolderManager::FolderManager() {}

std::vector<QString> FolderManager::creatDirectory(QString dstPath, std::vector<QString> dateList, std::vector<agvSrc> copyJobList)
{
    dstPath           = dstPath + "\\Logs\\";
    LPCSTR newDstPath = dstPath.toStdString().c_str();
    if (!CreateDirectoryA(newDstPath, NULL)) CreateDirectoryA(newDstPath, NULL);

    dstPathList.clear();
    for (auto date : dateList)
    {
        QString fullDstPath = "";
        LPCSTR  newFullPath;

        for (auto agv : copyJobList)
        {
            fullDstPath = dstPath + date + "\\";
            newFullPath = fullDstPath.toStdString().c_str();
            if (!CreateDirectoryA(newFullPath, NULL)) CreateDirectoryA(newFullPath, NULL);

            if (agv.mode == "HapCom")
            {
                fullDstPath = dstPath + date + "\\" + agv.name + "(" + agv.mode + ")";
                newFullPath = fullDstPath.toStdString().c_str();
                if (!CreateDirectoryA(newFullPath, NULL)) CreateDirectoryA(newFullPath, NULL);
                dstPathList.push_back(fullDstPath);
            }
            else
            {
                fullDstPath = dstPath + date + "\\" + agv.name + "(" + agv.mode + ")";
                newFullPath = fullDstPath.toStdString().c_str();
                if (!CreateDirectoryA(newFullPath, NULL)) CreateDirectoryA(newFullPath, NULL);
                dstPathList.push_back(fullDstPath);
            }
        }
    }
    return dstPathList;
}

std::vector<QString> FolderManager::getDstPathList() { return dstPathList; }
