import zipfile
from os.path import basename
import os

fileName = "pythonTaskList.txt"
print("\nDateininhalt: " + fileName + "\n")

file = open(fileName, 'r')

path = file.readline()
path = path.replace("\n", "")
print("PATH: " + path)


name = file.readline()
name = name.replace("\n", "")
print("NAME: " + name)

while name != "":
	dirPath = path + name
	
	myZip = zipfile.ZipFile(dirPath+".zip", "w")
	for eachFolder in os.listdir(dirPath + "\\"):
		print("\n"+ eachFolder)
		folderPath = dirPath+ "\\"+ eachFolder
		myZip.write(folderPath, basename(folderPath),zipfile.ZIP_DEFLATED)
		print(folderPath)
		for eachFile in os.listdir(folderPath):
			if eachFile.endswith(".log"):
				print(eachFile)
				filePath = folderPath + "\\" + eachFile
				myZip.write(filePath, eachFolder+"\\"+eachFile,zipfile.ZIP_DEFLATED)
				
	name = file.readline()
	name = name.replace("\n", "")
	
	if name != "":
		print("NAME: " + name)
	
file.close()	
myZip.close()