#include "customerselection.h"
#include "QCoreApplication"
#include "QFile"
#include "QMessageBox"
#include "QPushButton"

CustomerSelection::CustomerSelection()
{
    QString path = QCoreApplication::applicationDirPath();
    path.append(configPath);
    QFile file(path);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox errorBox;
        errorBox.setText("file (config.xml) does not exist!");
        errorBox.setIcon(QMessageBox::Warning);
        errorBox.addButton("OK", QMessageBox::YesRole);
        errorBox.exec();
    }

    QMessageBox msgBox;
    msgBox.setText("choose your customer");
    QPushButton* gf_dd_button        = msgBox.addButton("GF DD", QMessageBox::YesRole);
    QPushButton* siltronic_fg_button = msgBox.addButton("Siltronic FG", QMessageBox::YesRole);
    msgBox.exec();

    if (msgBox.clickedButton() == gf_dd_button)
        choosenCustomer = gf_dd;
    else if (msgBox.clickedButton() == siltronic_fg_button)
        choosenCustomer = siltronic_fg;
}
