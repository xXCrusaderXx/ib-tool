#include "logger.h"

Logger::Logger(QString LogName)
    : _filename(LogName)
{
    dstPath           = "\\Logs\\";
    LPCSTR newDstPath = dstPath.toStdString().c_str();
    if (!CreateDirectoryA(newDstPath, NULL)) CreateDirectoryA(newDstPath, NULL);
}

void Logger::out_string(QString out)
{
    QFile file(dstPath + _filename);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);

        do
            stream.readAll();
        while (not stream.atEnd());
        stream << out << endl;
    }
}

void Logger::out_agvSrc(std::vector<agvSrc> out)
{
    QFile file(dstPath + _filename);
    if (file.open(QIODevice::ReadWrite))
    {
        QTextStream stream(&file);

        do
            stream.readAll();
        while (not stream.atEnd());

        for (auto agv : out)
        {
            stream << agv.name + " | ";
            stream << agv.mode + " | ";
            stream << agv.path << endl;
        }
    }
}
