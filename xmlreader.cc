#include "xmlreader.h"
#include "QCoreApplication"
#include <QtCore/qdebug.h>

XmlReader::XmlReader(QString Path)
{
    QString path = QCoreApplication::applicationDirPath();
    path.append(Path);

    lookForPreDestination(path);
    lookForAgvData(path);
    makeLists();
}

void XmlReader::lookForPreDestination(QString& path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Cannot read file" << file.errorString();
        exit(0);
    }
    QXmlStreamReader myReader(&file);
    if (not myReader.readNextStartElement()) return;
    if (myReader.name() != "Configuration") return;

    while (not myReader.atEnd())
    {
        myReader.readNext();

        if (myReader.name() == "Destination")
        {
            savedDstPath = myReader.attributes().value("Path").toString();
            myReader.readNext();
        }
    }
    file.close();
}

void XmlReader::lookForAgvData(QString& path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Cannot read file" << file.errorString();
        exit(0);
    }
    QXmlStreamReader myReader(&file);
    if (not myReader.readNextStartElement()) return;
    if (myReader.name() != "Configuration") return;

    QString tmp = "";
    while (not myReader.atEnd())
    {
        myReader.readNextStartElement();

        if ((myReader.name() == "Customer") && (myReader.attributes().value("Name").toString() != ""))
        {
            customers.push_back(myReader.attributes().value("Name").toString());
            tmp = myReader.attributes().value("Name").toString();
        }
        if ((myReader.name() == "Source") && (myReader.attributes().value("Name").toString() != ""))
        {
            agvSrc agv;
            agv.customer = tmp;
            agv.name     = myReader.attributes().value("Name").toString();
            agv.path     = myReader.attributes().value("Path").toString();
            agv.mode     = myReader.attributes().value("Mode").toString();
            agvDataList.push_back(agv);
            myReader.readNext();
        }
    }
    file.close();
}

void XmlReader::makeLists()
{
    for (auto agv : agvDataList)
    {
        if (agv.mode == "HapCom") { hapcomPathList.push_back(agv); }
        else
        {
            xgvPathList.push_back(agv);
        }
    }
}
