#pragma once

#include "QXmlStreamWriter"
#include <structs.h>

class AgvXmlWriter
{
public:
    AgvXmlWriter();
    void setPathList(std::vector<agvSrc> pathList) { _pathList = pathList; }
    void setSavedDstPath(QString savedDstPath) { _savedDstPath = savedDstPath; }
    void writeXml();

private:
    std::vector<agvSrc> _pathList;
    QString             _savedDstPath = "";
};
