#pragma once

#include "QFile"
#include "QXmlStreamReader"
#include <structs.h>

class XmlReader
{
public:
    XmlReader(QString Path);
    std::vector<agvSrc>  getHapComPathList() { return hapcomPathList; }
    std::vector<agvSrc>  getXgvPathList() { return xgvPathList; }
    QString              getSavedDstPath() { return savedDstPath; }
    std::vector<QString> getCustomers() { return customers; }

    std::vector<agvSrc>  agvDataList;
    std::vector<agvSrc>  hapcomPathList;
    std::vector<agvSrc>  xgvPathList;
    std::vector<QString> customers;
    QString              savedDstPath = "";
    QXmlStreamReader     myReader;

private:
    void lookForAgvData(QString& path);
    void lookForPreDestination(QString& path);
    void makeLists();
};
