#pragma once

#include <QString>

struct date
{
    QString day;
    QString month;
    QString year;
};

struct fileCopyData
{
    QString src_path;
    QString dst_path;
    QString dstFolder;
};

struct agvSrc
{
    QString customer;
    QString name;
    QString mode;
    QString path;
};
